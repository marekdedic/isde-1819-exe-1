import numpy as np;
from pandas import read_csv;

def load_data():
    data = np.array(read_csv('data/mnist_data.csv'));
    return (data[:, 1:], data[:, 0]);

def split_data(X, y, tr_fraction=0.5):
    """Split the data X,y into two random subsets."""
    raise NotImplementedError


def count_digits(y):
    """Count the number of elements in each class."""
    raise NotImplementedError


def fit(Xtr, ytr):
    """Compute the average centroid for each class."""
    raise NotImplementedError
