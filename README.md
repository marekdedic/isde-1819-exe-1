# Exercise - 24 October 2018

Each working group must implement the functions illustrated
 in the previous lessons regarding the loading of MNIST DIGITS dataset
 and the computation of the centroids using Git tools for shared development.

#### The goal is to pass the automated tests setup in the Gitlab project for `master` branch.

If all the functions have been correctly implemented, you will find a `passed` green badge
 under the CI/CD -> Pipelines menu on the left of the gitlab.com project page.
If the tests are failing, a red `failed` badge will be displayed in the same page.

In particular each group must develop:
- Function(s) for loading mnist dataset (available in `data/mnist_data.csv`)
- `split_data(X, y, tr_fraction=0.5)`, split data (return 2 sets, training and test)
- `count_digits(y)`, return the number of elements in each class for training set
- `fit(Xtr, ytr)`, compute and return the average centroid for each class for training set

**All functions must be added to the `fun_utils.py` script**,
 which already contains the signature of the required functions.
 
Tests are available inside `tests/test.py` script.

By default the following libraries can be used:
 - numpy
 - matplotlib
 - pandas

Change the `requirements.txt` file by adding the name of any new dependency if necessary.

### Workflow details:
1. The team leader *only* should fork the current project and provide the 
 repository url to other team members. All team members can clone the project locally.
2. Each group should **discuss** how to develop each function
 (example: if any extra function is needed, what should return, ...).
3. Each group should then **discuss** which function each team member should implement.
4. Development of each function must be done in a **specific branch**,
 which must be pushed to the remote after creation.
 **No direct commits into the `master` branch**
5. Each team member is suggested to commit local changes to remote
 as often as possible, so that other team members can see the progress
 on the code via the gitlab.com project page.
6. During the development, team members are encouraged to discuss code
 implementation and possible problems.
7. If needed, changes from other feature branches can be merged into a specific feature branch.
 Use any Git tool needed to correctly integrate the changes from multiple branches.
8. After the development of a specific feature has been completed, 
 the relative branch should be merged into `master`.
9. The CI/CD -> Pipelines should now be consulted to see the result of the automated tests.
 Depending on the load of the test runners of gitlab.com, the pipeline could show 
 the `pending` status for *quite a while*. Just wait until a runner picked the job.
 As an alternative, the test script can be manually run by calling `PYTHONPATH=. python tests/test.py`.
10. If any change to the code is necessary, should be developed in the branch
 relative to the specific feature and merged into master to see if tests are then passing.


## Details about functions and variables
Common variables:
- y: labels (one for each image) (numpy array)
- X: Set of images. numpy array of size (nImages, nFeatures). Each row represents an image.

```python
split_data(X, y, tr_fraction):
    """Split the data X,y into two random subsets.
    input:
        X: Set of images
        y: Labels
        tr_fraction: float, percentage of samples to put in the training set.
            If necessary, number of samples in the training set is rounded to
            the lowest integer number.
    output:
        Xtr:  Set of images (numpy array, training set)
        ytr: Labels (numpy array, training set)
        Xts:  Set of images (numpy array, test set)
        yts: Labels (numpy array, test set)
    
    """


count_digits(y):
    """Count the number of elements in each class.
    input:
        y: Labels
    output: number of elements in each class (numpy array of int)
    
    """


fit(Xtr, ytr):
    """Compute the average centroid for each class.
    
    input:
        X: Set of images (training set)
        y: Labels (training set)
    output: numpy array of size (nClasses, nFeatures). Each row represents a centroid.
    
    """
    
	
```
